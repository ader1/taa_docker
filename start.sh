#!/bin/sh

echo --------------------- initialisation of containers docker ------------------- 
sudo docker-compose stop
sudo docker-compose rm -f

echo --- build of image docker taa_app_spring 
sudo docker build -t taa_app_spring .

echo -------------------- build of image docker mysql -----------------------
sudo docker pull mysql

echo -------------------- build of image docker nginx ---------------------
sudo docker build -t nginx nginx/

echo -------------------- execution of containers docker -------------------
sudo docker-compose up

