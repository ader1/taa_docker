# TP Docker

## Etudiants

Cette application est crée par Ouhammouch Salma & Assal Mohamed Rida 
Etudiants en Master2 MITIC

## Lancement du programme 
Pour lancer le programme, suivez les étapes suivantes:

1 - telecharger le projet.

2 - ouvrir un terminal. 

3 - aller sur le repertoire contenant le projet. 

4 - donner le droit exécution pour le script strat.sh avec cette command "chmod +rwx start.sh ".

5 - exécuter le script "./start.sh" ce script contient une liste de commandes de docker pour déployer notre   application(nginx , bdd, 3 serveur d'application ).

6 - lancer l'application angularsJs ou GWT (Client Side) pour communiquer avec le serveur TAA de docker.